
package rs.itcentar.mynotepad;

import java.awt.HeadlessException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

public class TabWithX extends javax.swing.JPanel {

    private JTabbedPane tpFiles;
    private TextAreaPanel panel;

    public TabWithX(JTabbedPane tpFiles, TextAreaPanel panel) {
        initComponents();
        this.panel = panel;
        this.tpFiles = tpFiles;
        this.lTitle.setText(this.panel.getFileName());
    }

    public void changedName(String name) {
        lTitle.setText(name + "*");
    }

    public void savedName(String name) {
        lTitle.setText(name);
    }

    public JLabel getTabTitle() {
        return lTitle;
    }
    
    public void showSaveDialog() throws HeadlessException {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.addChoosableFileFilter(new OpenFileFilter("*.txt", "*.txt"));
        int ret = fc.showSaveDialog(TabWithX.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            
            try {
                try (FileWriter fw = new FileWriter(file);
                        BufferedWriter bw = new BufferedWriter(fw)) {
                    
                    TextAreaPanel textAreaPanel = (TextAreaPanel) tpFiles.getSelectedComponent();
                        bw.write(textAreaPanel.getText());
                        int index = tpFiles.getSelectedIndex();
                        tpFiles.removeTabAt(index);
                        tpFiles.insertTab(file.getName(), null, textAreaPanel, null, index);
                        tpFiles.setTabComponentAt(index, textAreaPanel.getTabWithX());// nece da radi a treba!
                }
            } catch (IOException ex) {
                Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lTitle = new javax.swing.JLabel();
        bClose = new javax.swing.JButton();

        setOpaque(false);
        setRequestFocusEnabled(false);

        lTitle.setFocusable(false);

        bClose.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        bClose.setText("X");
        bClose.setBorderPainted(false);
        bClose.setContentAreaFilled(false);
        bClose.setDefaultCapable(false);
        bClose.setFocusPainted(false);
        bClose.setFocusable(false);
        bClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        bClose.setMaximumSize(new java.awt.Dimension(12, 12));
        bClose.setMinimumSize(new java.awt.Dimension(12, 12));
        bClose.setPreferredSize(new java.awt.Dimension(12, 12));
        bClose.setRequestFocusEnabled(false);
        bClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClose, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCloseActionPerformed
        if (!panel.isChanged()) {
            tpFiles.remove(panel);
        } else {
            int opcija = JOptionPane.showConfirmDialog(TabWithX.this,
                            String.format("Are you sure you want to exit without saving?"),
                            "File content has been changed!",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcija == JOptionPane.YES_OPTION) {
                tpFiles.remove(panel);
            } else {
                showSaveDialog();
            }
        }

    }//GEN-LAST:event_bCloseActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClose;
    private javax.swing.JLabel lTitle;
    // End of variables declaration//GEN-END:variables
}

package rs.itcentar.mynotepad;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

public class NotepadFrame extends javax.swing.JFrame {
    
    private File file;
    
    public NotepadFrame() {
        initComponents();
        
        tpFiles.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                System.out.println(tpFiles.getSelectedIndex());
                TextAreaPanel textAreaPanel = (TextAreaPanel) tpFiles.getSelectedComponent();
                if (textAreaPanel != null) {
                    if (textAreaPanel.getFile() != null) {
                        lStatusBar.setText(textAreaPanel.getFilePath());
                    } else {
                        lStatusBar.setText("");
                    }
                } else {
                    lStatusBar.setText("");
                }
            }
        });
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lStatusBar = new javax.swing.JLabel();
        tpFiles = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        mbFile = new javax.swing.JMenu();
        miNew = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miSaveAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        miClose = new javax.swing.JMenuItem();
        mbFormat = new javax.swing.JMenu();
        mcbWordWrap = new javax.swing.JCheckBoxMenuItem();
        mbLanguages = new javax.swing.JMenu();
        rmbiJava = new javax.swing.JRadioButtonMenuItem();
        rmbiJavaScript = new javax.swing.JRadioButtonMenuItem();
        rmbiC = new javax.swing.JRadioButtonMenuItem();
        rmbiCPlusPlus = new javax.swing.JRadioButtonMenuItem();
        rmbiPython = new javax.swing.JRadioButtonMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        rmbiDefault = new javax.swing.JRadioButtonMenuItem();
        mbHelp = new javax.swing.JMenu();
        miAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("My Notepad 1.0.0");

        lStatusBar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        mbFile.setText("File");

        miNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        miNew.setText("New");
        miNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miNewActionPerformed(evt);
            }
        });
        mbFile.add(miNew);

        miOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        miOpen.setText("Open");
        miOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOpenActionPerformed(evt);
            }
        });
        mbFile.add(miOpen);

        miSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        miSave.setText("Save");
        miSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveActionPerformed(evt);
            }
        });
        mbFile.add(miSave);

        miSaveAs.setText("Save As");
        miSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveAsActionPerformed(evt);
            }
        });
        mbFile.add(miSaveAs);
        mbFile.add(jSeparator1);
        mbFile.add(jSeparator2);

        miClose.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, java.awt.event.InputEvent.CTRL_MASK));
        miClose.setText("Close");
        miClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCloseActionPerformed(evt);
            }
        });
        mbFile.add(miClose);

        jMenuBar1.add(mbFile);

        mbFormat.setText("Format");

        mcbWordWrap.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        mcbWordWrap.setText("WordWrap");
        mcbWordWrap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mcbWordWrapActionPerformed(evt);
            }
        });
        mbFormat.add(mcbWordWrap);

        jMenuBar1.add(mbFormat);

        mbLanguages.setText("Languages");

        buttonGroup1.add(rmbiJava);
        rmbiJava.setText("Java");
        rmbiJava.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiJavaActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiJava);

        buttonGroup1.add(rmbiJavaScript);
        rmbiJavaScript.setText("JavaScript");
        rmbiJavaScript.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiJavaScriptActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiJavaScript);

        buttonGroup1.add(rmbiC);
        rmbiC.setText("C");
        rmbiC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiCActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiC);

        buttonGroup1.add(rmbiCPlusPlus);
        rmbiCPlusPlus.setText("C++");
        rmbiCPlusPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiCPlusPlusActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiCPlusPlus);

        buttonGroup1.add(rmbiPython);
        rmbiPython.setText("Python");
        rmbiPython.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiPythonActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiPython);
        mbLanguages.add(jSeparator3);

        buttonGroup1.add(rmbiDefault);
        rmbiDefault.setSelected(true);
        rmbiDefault.setText("Default");
        rmbiDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmbiDefaultActionPerformed(evt);
            }
        });
        mbLanguages.add(rmbiDefault);

        jMenuBar1.add(mbLanguages);

        mbHelp.setText("Help");

        miAbout.setText("About");
        mbHelp.add(miAbout);

        jMenuBar1.add(mbHelp);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lStatusBar, javax.swing.GroupLayout.DEFAULT_SIZE, 1024, Short.MAX_VALUE)
            .addComponent(tpFiles)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tpFiles, javax.swing.GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lStatusBar, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOpenActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.addChoosableFileFilter(new OpenFileFilter("*.txt", "Text file"));
        int ret = fc.showOpenDialog(NotepadFrame.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            
            if (HistoryManager.isFilePathOpened(file.getPath())) {
                TextAreaPanel panel = HistoryManager.getPanelForFilePath(file.getPath());
                tpFiles.setSelectedComponent(panel);
            } else {
                try {
                    FileReader fr = new FileReader(file);
                    BufferedReader br = new BufferedReader(fr);
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append(System.lineSeparator());
                    }
                    
                    TextAreaPanel textAreaPanel = new TextAreaPanel(tpFiles, file);
                    tpFiles.addTab(file.getName(), textAreaPanel);
                    tpFiles.setTabComponentAt(tpFiles.indexOfComponent(textAreaPanel), textAreaPanel.getTabWithX());
                    textAreaPanel.setText(sb.toString());
                    tpFiles.setSelectedIndex(tpFiles.getTabCount() - 1);
                    HistoryManager.insert(textAreaPanel.getFilePath(), textAreaPanel);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            lStatusBar.setText(file.getPath());
            setTitle(getTitle() + " - " + file.getPath());
        }
    }//GEN-LAST:event_miOpenActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveActionPerformed
        TextAreaPanel tabPanel = (TextAreaPanel) tpFiles.getSelectedComponent();
        if (tabPanel.isChanged()) {
            if (tabPanel.getFile() != null) {
                // save on file path
                tabPanel.save();
            } else {
                showSaveDialog();
            }
        }
    }//GEN-LAST:event_miSaveActionPerformed
    
    public void showSaveDialog() throws HeadlessException {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.addChoosableFileFilter(new OpenFileFilter("*.txt", "*.txt"));
        int ret = fc.showSaveDialog(NotepadFrame.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            
            try {
                try (FileWriter fw = new FileWriter(file);
                        BufferedWriter bw = new BufferedWriter(fw)) {
                    
                        TextAreaPanel textAreaPanel = (TextAreaPanel) tpFiles.getSelectedComponent();
                        bw.write(textAreaPanel.getText());
                        int index = tpFiles.getSelectedIndex();
                        tpFiles.removeTabAt(index);
                        tpFiles.insertTab(file.getName(), null, textAreaPanel, null, index);
                        tpFiles.setTabComponentAt(index, textAreaPanel.getTabWithX()); // trebalo bi da radi!
                        
                }
            } catch (IOException ex) {
                Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void miCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCloseActionPerformed
        saveChangedFiles();
        System.exit(0);
    }//GEN-LAST:event_miCloseActionPerformed

    private void miNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miNewActionPerformed
            TextAreaPanel panel = new TextAreaPanel(tpFiles, null);
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
            tpFiles.addTab(TextAreaPanel.DEFAULT_FILE_NAME, panel);
            tpFiles.setTabComponentAt(tpFiles.indexOfComponent(panel), panel.getTabWithX());
            tpFiles.setSelectedIndex(tpFiles.getTabCount() - 1);
            HistoryManager.insert(null, panel);
    }//GEN-LAST:event_miNewActionPerformed

    private void miSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveAsActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        if (!openedFiles.isEmpty()) {
            showSaveDialog();
        } else {
            lStatusBar.setText("Nema fajla za obradu!");
        }
    }//GEN-LAST:event_miSaveAsActionPerformed

    private void mcbWordWrapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mcbWordWrapActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            panel.toggleWordWrap(mcbWordWrap.isSelected());
        }        
    }//GEN-LAST:event_mcbWordWrapActionPerformed

    private void rmbiJavaScriptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiJavaScriptActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }    
    }//GEN-LAST:event_rmbiJavaScriptActionPerformed

    private void rmbiJavaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiJavaActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }
    }//GEN-LAST:event_rmbiJavaActionPerformed

    private void rmbiCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiCActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }
    }//GEN-LAST:event_rmbiCActionPerformed

    private void rmbiCPlusPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiCPlusPlusActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }
    }//GEN-LAST:event_rmbiCPlusPlusActionPerformed

    private void rmbiPythonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiPythonActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }
    }//GEN-LAST:event_rmbiPythonActionPerformed

    private void rmbiDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rmbiDefaultActionPerformed
        Map<String, TextAreaPanel> openedFiles = HistoryManager.getOpenedFiles();
        for (TextAreaPanel panel : openedFiles.values()) {
            RSyntaxTextArea textArea = panel.getTaText();
            textArea.getDocument().removeDocumentListener(panel.getDocumentListener());
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
            textArea.getDocument().addDocumentListener(panel.getDocumentListener());
        }
    }//GEN-LAST:event_rmbiDefaultActionPerformed
    
    private void saveChangedFiles() {
        for (int i = 0; i < tpFiles.getTabCount(); i++) {
            TextAreaPanel panel = (TextAreaPanel) tpFiles.getComponentAt(i);
            if (panel.isChanged()) {
                if (!(panel.getFile() == null)) {
                    tpFiles.setSelectedIndex(i);
                    int opcija = JOptionPane.showConfirmDialog(NotepadFrame.this,
                            String.format("Would you like to save '%s'?!",
                                    panel.getFileName()), "File content has been changed!",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcija == JOptionPane.YES_OPTION) {
                        panel.save();
                    }
                } 
                 else {
                    tpFiles.setSelectedIndex(i);
                    int opcija = JOptionPane.showConfirmDialog(NotepadFrame.this,
                            String.format("Would you like to save '%s'?!",
                                    panel.getFileName()), "File content has been changed!",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcija == JOptionPane.YES_OPTION) {
                        showSaveDialog();
                    }
                }
            }
        }
    }
    
    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NotepadFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NotepadFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NotepadFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NotepadFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NotepadFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JLabel lStatusBar;
    private javax.swing.JMenu mbFile;
    private javax.swing.JMenu mbFormat;
    private javax.swing.JMenu mbHelp;
    private javax.swing.JMenu mbLanguages;
    private javax.swing.JCheckBoxMenuItem mcbWordWrap;
    private javax.swing.JMenuItem miAbout;
    private javax.swing.JMenuItem miClose;
    private javax.swing.JMenuItem miNew;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem miSaveAs;
    private javax.swing.JRadioButtonMenuItem rmbiC;
    private javax.swing.JRadioButtonMenuItem rmbiCPlusPlus;
    private javax.swing.JRadioButtonMenuItem rmbiDefault;
    private javax.swing.JRadioButtonMenuItem rmbiJava;
    private javax.swing.JRadioButtonMenuItem rmbiJavaScript;
    private javax.swing.JRadioButtonMenuItem rmbiPython;
    private javax.swing.JTabbedPane tpFiles;
    // End of variables declaration//GEN-END:variables
}

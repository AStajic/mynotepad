package rs.itcentar.mynotepad;

import java.awt.BorderLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

public class TextAreaPanel extends javax.swing.JPanel {

    private JTabbedPane tpFiles;
    private boolean changed = false;
    private File file;
    private TabWithX tabWithX;
    private RSyntaxTextArea taText;

    private DocumentListener documentListener;
    public static final String DEFAULT_FILE_NAME = "New File";

    public TextAreaPanel(JTabbedPane tpFiles, File file) {
        this.tpFiles = tpFiles;
        this.file = file;
        this.tabWithX = new TabWithX(tpFiles, this);
        initComponents();

        setLayout(new BorderLayout());
        taText = new RSyntaxTextArea();
        taText.setCodeFoldingEnabled(true);
        setSyntaxStyle(file);
        RTextScrollPane sp = new RTextScrollPane(taText);
        add(sp);

        documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                change();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                change();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                change();
            }

            private void change() {
                changed = true;
                int index = tpFiles.indexOfComponent(TextAreaPanel.this);
                String title = tpFiles.getTitleAt(index);
                tpFiles.setTitleAt(index, "<html><i>" + title + "</i></html>");
                JLabel tabTitle = tabWithX.getTabTitle();
                tabTitle.setText(tpFiles.getTitleAt(index));
            }
        };
    }

    public TabWithX getTabWithX() {
        return tabWithX;
    }

    public JTabbedPane getTpFiles() {
        return tpFiles;
    }

    public void setText(String text) {
        taText.getDocument().removeDocumentListener(documentListener);
        taText.setText(text);
        taText.setCaretPosition(0);
        taText.getDocument().addDocumentListener(documentListener);
    }

    public RSyntaxTextArea getTaText() {
        return taText;
    }

    public String getText() {
        return taText.getText();
    }

    public File getFile() {
        return file;
    }

    public String getFilePath() {
        return file.getPath();
    }

    public String getFileName() {
        if (file == null) {
            return DEFAULT_FILE_NAME;
        }
        return file.getName();
    }

    public boolean isChanged() {
        return changed;
    }

    public void resetChanged() {
        changed = false;
    }
    
    public DocumentListener getDocumentListener() {
        return documentListener;
    }

    public void save() {
        try {
            try (FileWriter fw = new FileWriter(file);
                    BufferedWriter bw = new BufferedWriter(fw)) {

                TextAreaPanel textAreaPanel = (TextAreaPanel) tpFiles.getSelectedComponent();
                bw.write(textAreaPanel.getText());// koji sam tab odabrao i za njega vadi text
            }
        } catch (IOException ex) {
            Logger.getLogger(NotepadFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void toggleWordWrap(boolean selected) {
        taText.setLineWrap(changed);
        taText.setWrapStyleWord(changed);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void setSyntaxStyle(File file) {
        if (file == null) {
            taText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
        } else {
            String name = file.getName();
            int end = name.length();
            int start = name.indexOf(".") + 1;
            String extension = file.getName().substring(start, end);

            switch (extension) {
                case "java":
                    taText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                    break;
                case "txt":
                    taText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
                    break;
                default:
                    taText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
                    break;
            }
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}

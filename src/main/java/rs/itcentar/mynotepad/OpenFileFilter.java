
package rs.itcentar.mynotepad;

import java.io.File;
import javax.swing.filechooser.FileFilter;


public class OpenFileFilter extends FileFilter {
    
   String description = "";
   String extension = " ";
    
    public OpenFileFilter(String extension) {
        this.extension = extension;
    }
    
    public OpenFileFilter(String extension, String description) {
        this.extension = extension;
        this.description = description;
    }
    
    @Override
    public boolean accept(File f) {
        if (f.isDirectory())
            return true;
        return (f.getName().toLowerCase().endsWith(extension));
        }

    @Override
    public String getDescription() {
        return description;
    }
    
}

package rs.itcentar.mynotepad;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Java
 */
public class HistoryManager {

    private static Map<String, TextAreaPanel> openedFiles = new HashMap<>();

    public static Map<String, TextAreaPanel> getOpenedFiles() {
        return openedFiles;
    }

    public static void insert(String filePath, TextAreaPanel panel) {
        openedFiles.put(filePath, panel);
    }

    public static TextAreaPanel getPanelForFilePath(String filePath) {
        return openedFiles.get(filePath);
    }

    public static boolean isFilePathOpened(String filePath) {
        return openedFiles.containsKey(filePath);
    }
    
    
}
